package com.my.study;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		PrintWriter out =  response.getWriter();
		out.print("hello world ! from servlet");
		out.flush();
		out.close();
	}

}
